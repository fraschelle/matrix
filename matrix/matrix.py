#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Matrix object allow pedagogical representation of many of its calculations.
"""

from typing import List
from math import sqrt
from fractions import Fraction

class Matrix(List[list]):
    """Representation of squared matrices with only integer coefficients."""

    def __init__(self, matrix=[[1,0],[0,1]], verbose=True):
        """coeff_list is a list of integer coefficients, whose total number 
        must correspond to a square, giving the dimension of the corresponding matrix"""
        self.shape = (len(matrix), len(matrix[0]))
        if not len(matrix) == self.shape[0]:
            raise ValueError("matrix should be compatible with shape")
        if not all(len(line)==self.shape[1] for line in matrix):
            raise ValueError("matrix should be compatible with shape")
        self.matrix = matrix
        self.verbose = bool(verbose)
        return None
    
    def __check_matrix(self, matrix):
        """Raise a ValueError in case `matrix` is not a valid Matrix instance."""
        if not isinstance(matrix, self.__class__):
            raise ValueError("Accept only Matrix instance")
        return None
    
    def __check_shape(self, matrix):
        """Raise a ValueError in case `matrix` has different shape than self."""
        if self.shape != matrix.shape:
            raise ValueError("the two Matrix instances must have same shape")        
    
    def __getitem__(self, position):
        i,j = position
        return self.matrix[i][j]

    def __setitem__(self, position, value):
        i,j = position
        self.matrix[i][j] = value
        return None
    
    def transpose(self,):
        """Construct the transpose of the matrix."""
        lines = [[self[i,j] for i in range(self.shape[0])]
                 for j in range(self.shape[1])]
        return self.__class__(matrix=lines)
    
    def construct_empty(self, n, m):
        """Construct a Matrix filled with 0, with a (n,m) shape."""
        empty = [[0]*n for _ in range(m)]
        return self.__class__(matrix=empty)
    
    def construct_identity(self, n, m):
        """Construct the identity Matrix of shape (n,m)"""
        identity = [[0]*min(i,m) + [1]*(i<m) + [0]*(m-i-1)
                    for i in range(n)]
        return self.__class__(matrix=identity)
    
    def __add__(self, matrix):
        """Addition of two SquareMatrix"""
        self.__check_matrix(matrix)
        newmat = self.construct_empty(*self.shape)
        for i in range(self.shape[0]):
            for j in range(self.shape[1]):
                newmat[i,j] = self[i,j] + matrix[i,j]
        return newmat

    def __sub__(self, matrix):
        """Difference of two SquareMatrix."""
        self.__check_matrix(matrix)
        newmat = self.construct_empty(*self.shape)
        for i in range(self.shape):
            for j in range(self.shape):
                newmat[i,j] = self[i,j] - matrix[i,j]
        return newmat

    def __mul__(self, number):
        """Multiplication of a SquareMatrix by an other of same shape, 
        or a number (anything with __mul__ allowed with the content of the Matrix)."""
        newmat = self.construct_empty(*self.shape)
        for i in range(self.shape):
            for j in range(self.shape):
                newmat[i,j] = self[i,j] * number
        return newmat
    
    def __div__(self, number):
        """Division of a SquareMatrix by an integer."""
        newmat = self.construct_empty(*self.shape)
        if isinstance(matrix, self.__class__):
            raise NotImplementedError
        elif '__mul__' in dir(matrix):
            for i in range(self.shape):
                for j in range(self.shape):
                    newmat[i,j] = Fraction(self[i,j], matrix)
        return newmat
    
    def __matmul__(self, matrix):
        """Multiplication of a SquareMatrix by an other of same shape, 
        or a number (anything with __mul__ allowed with the content of the Matrix)."""
        newmat = self.construct_empty(*self.shape)
        if not isinstance(matrix, self.__class__):
            raise TypeError("entity must contain a __mul__ method")
        if not self.shape[1] == matrix.shape[0]:
            raise ValueError("nb of rows of B must match nb of columns of A in A@B")
        self.__check_matrix(matrix)
        for i in range(self.shape[0]):
            for j in range(matrix.shape[1]):
                newmat[i,j] = sum(self[i,k] * matrix[k,j] 
                                  for k in range(self.shape[1]))
        return newmat

    def __repr__(self):
        return self.__class__.__name__ + "\n" + str(self)

    def __str__(self):
        return '\n'.join('\t'.join(str(round(c,2)).rjust(4) for c in line)
                         for line in self.matrix)
    
    def __eq__(self, matrix):
        """Return True if all coefficients are equal among the two entities, otherwise returns False."""
        self.__check_matrix(matrix)
        self.__check_shape(matrix)
        bools = [self[i,j] == matrix[i,j] 
                 for i in range(self.shape[0])
                 for j in range(self.shape[1])]
        return all(bools)
    
    def trace(self,):
        """Calculate the trace of the Matrix"""
        res = sum(self[i,i] for i in range(min(self.shape)))
        if self.verbose:
            string = str()
            for i in range(min(self.shape)):
                symbol = " + " if self[i,i]>=0 else " - "
                string += symbol + str(abs(round(self[i,i], 2)))
            print(string + " = {}".format(round(res, 2)))
        return res
    
    def determinant(self, flag=1):
        if self.shape[0] != self.shape[1]:
            raise ValueError("method applies only to square matrices")
        if self.shape[0] == 1:
            return self[0,0]
        matrix = self.__class__(self.matrix)
        res, string = 0, ""
        for _ in range(self.shape[0]):
            sub_matrix = self.__class__([m[1:] for m in matrix.matrix[1:]])            
            m00 = matrix[0,0]
            sub = sub_matrix.determinant(flag=flag)
            if self.verbose:
                symbol = "-" if flag == -1 else "+"
                string += f"{round(res,2)} {symbol} ({round(m00,2)} x {round(sub,2)})"
            res += flag * m00 * sub
            flag = - flag
            # put the first line at the bottom, and start over
            matrix = self.__class__(matrix.matrix[1:] + matrix.matrix[:1])
            if self.verbose:
                string += f" = {round(res,2)}\n"
                string += f"flip the matrix to:\n{matrix}\n"
                if _ != self.shape[0]-1:
                    string += "next step --->\n"
        if self.verbose:
            print(string)
        return res
    